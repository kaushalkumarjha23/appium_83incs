package testBMS;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class GenerateExtentReports {

	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	ExtentTest test;
	
	@BeforeSuite
	public void startReport()
	{
		
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir")+"/test-output/MyReports.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		
		extent.setSystemInfo("OS", "Android");
		extent.setSystemInfo("Environment", "QA");
		
		htmlReporter.config().setDocumentTitle("Sample Report");
		htmlReporter.config().setReportName("My Reports");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.DARK);
		
	}

  @AfterMethod
  public void getResult(ITestResult result)
  {
	  if(result.getStatus()==ITestResult.FAILURE)
	  {
		  test.fail(MarkupHelper.createLabel(result.getName()+ "Test Case Failed", ExtentColor.RED));
		  test.fail(result.getThrowable());
	  }
	  else if(result.getStatus()==ITestResult.SUCCESS)
	  {
		  test.pass(MarkupHelper.createLabel(result.getName()+ "Test Case Success", ExtentColor.GREEN));  
	  }
	  else 
	  {
		  test.skip(MarkupHelper.createLabel(result.getName()+ "Test Case Skipped", ExtentColor.YELLOW)); 
		  test.skip(result.getThrowable());
	  }
  }
  @AfterSuite
  public void tearDown()
  {
	  extent.flush();
  }
  
}
