 package objects;

import org.openqa.selenium.*;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Launch {

      private static MobileElement element = null;

      public static MobileElement runApp(AndroidDriver driver){

      element = (MobileElement) driver.findElement(By.id("in.amazon.mShop.android.shopping:id/sign_in_button"));
      
      return element;
      }
}